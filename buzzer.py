import time
import RPi.GPIO as GPIO


def buzzer(pin,tiempo):
	start=time.time()
	restante=100
	while (restante>=0):
		GPIO.output(pin,1)
		actual=time.time()
		restante=tiempo+start-actual
		#print "Segundos de sonido restantes:",restante
	GPIO.output(pin,0)
