# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#           Libreria notificacion de estados de bateria
#####################################################################
import time
import RPi.GPIO as GPIO
import credentials
import urllib
import funciones


estoma = credentials.Get_Estoma_Info()[0]
estomaInfo = credentials.Get_Estoma_Info()

SUBJECT_CARGADOR= "Cargador"
BODY_CARGADOR = "El cargador del equipo " + estoma + " se ha desconectado"

print estoma


# Setear GPIO en configuracion Broadcom y definir los pines de pulsadores
GPIO.setmode(GPIO.BCM)
cargador = 16
descarga = 23
# Setear el modo de operacion de los pines
GPIO.setwarnings(False)
GPIO.setup(cargador,GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(descarga,GPIO.IN, pull_up_down=GPIO.PUD_UP)


def carga(cargador):
        print "Desconexion"
        if GPIO.input(cargador) == 0:
            funciones.Web_noti(SUBJECT_CARGADOR, BODY_CARGADOR)
            print "Se desconecto el cargador"


GPIO.add_event_detect(cargador, GPIO.FALLING, callback=carga, bouncetime=300)
