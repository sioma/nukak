__author__ = 'cristianrojas'
webcredentials=[]
estomaInfo=[]
estomaNombre = 'ESTOMATESTB'
loteFiltros = 4
def Get_Siomapp_Credentials():
    webcredentials.append("C0l0mb14S10m4")
    webcredentials.append("bandejas")

    return webcredentials

def Get_Estoma_Info():
    global estomaNombre
    estomaInfo.append(estomaNombre)
    return estomaInfo

def Get_Parametros_Filtro():
    global loteFiltros
    parametros = []
    if loteFiltros == 1:
        lenVec = 15
        lenVecFilP = 6
        alfa = 0.01
        alfaM = 0.1
        alfaG = 0.5
        difG = 0.5
        difM = 0.06
        resolucion = 0.05
    elif loteFiltros == 2 or loteFiltros == 3:
        lenVec = 15
        lenVecFilP = 6
        alfa = 0.0001
        alfaM = 0.0001
        alfaG = 0.1
        difG = 0.9
        difM = 0.08
        resolucion = 0.05
    elif loteFiltros == 4:
        lenVec = 15
        lenVecFilP = 6
        alfa = 0.005
        alfaM = 0.005
        alfaG = 0.1
        difG = 0.3
        difM = 0.08
        resolucion = 0.05
    parametros.append(lenVec)
    parametros.append(lenVecFilP)
    parametros.append(alfa)
    parametros.append(alfaM)
    parametros.append(alfaG)
    parametros.append(difG)
    parametros.append(difM)
    parametros.append(resolucion)
    return parametros