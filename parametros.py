# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#           Libreria de parametros para Nukak                       #
#####################################################################
###############         Importacion de librerias   ##################
import LCD
import time
import RPi.GPIO as GPIO
import credentials
import funciones
import os
import sys

#####################################################################
######               Declaracion de variables                 #######
embarque = 18.6
posicion = 0
factor = 2.205
estado = 0
calibracion = 0
nombre = " "
pesos = " "
peso = 0
alerta = False
PERIODOVALIDACION = 1
auxCont = 0
#####################################################################
######               Declaracion de pines                     #######
GPIO.setmode(GPIO.BCM)
botonUp = 26
botonDown = 12
GPIO.setwarnings(False)
GPIO.setup(botonUp, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(botonDown, GPIO.IN, pull_up_down=GPIO.PUD_UP)


#####################################################################
######               Metodos de inicializacion                #######

#### Creacion de la tabla de embarques y calibraciones
funciones.Conect_Db_Embarques()
funciones.Conect_db_Calibracion()
funciones.Conect_db_parametros()

#### Validacion de validacion diaria
lastValidacion = funciones.Get_Last_Validacion()
fechaHoy = funciones.Actualizar_hora(dia=1)
print "Ultima Validacion: ", lastValidacion, "Fecha actual: ", fechaHoy
if lastValidacion is not None:
    print "Diferencia: ", (fechaHoy - lastValidacion.date()).days


if lastValidacion is None or (fechaHoy - lastValidacion.date()).days >= PERIODOVALIDACION:
    calibracion = 2
else:
    print "Validacion de equipo se encuntra al dia"

#### Sincronizacion de bases de datos
try:
    online = funciones.Test_online()
except:
    funciones.RGB("Morado", 1)

estomaInfo = credentials.Get_Estoma_Info()
estomaId = estomaInfo[0]


if online:
    LCD.lcd_clean()
    LCD.lcd_fondo(100, 100, 100)
    LCD.lcd_string("Sincronizando...", 1, 1)
    try:
        LCD.lcd_string("Sincronizando...", 2, 2)
        LCD.lcd_string("Lb", 2, 2)
        #### Actualizacion de estado de lb
        response = funciones.Web_conex("lb", estomaId, timeout=8, intentos=3)
        print response
        lb = int(response['lb'])
        funciones.Set_parametro("lb", lb)
        print "Estado de libras actualizado:", lb
    except Exception, e:
        print "ERROR ACTUALIZANDO LB: " + repr(e)
        lb = int(funciones.Get_parametro("lb"))

    try:
        LCD.lcd_string("Sincronizando...", 2, 2)
        LCD.lcd_string("Tara", 2, 2)
        #### Actualizacion de tara
        response = funciones.Web_conex("tara", estomaId, timeout=8, intentos=3)
        tara = float(response['tara'])
        funciones.Set_parametro("tara", tara)
        print "Tara actualizada: ", tara
    except Exception, e:
        print "ERROR ACTUALIZANDO TARA: " + repr(e)
        tara = float(funciones.Get_parametro("tara"))

    try:
        LCD.lcd_string("Sincronizando...", 2, 2)
        LCD.lcd_string("Patron", 2, 2)
        #### Actualizar valor de patron de calibracion
        response = funciones.Web_conex("patron", estomaId, timeout=8, intentos=3)
        patron = float(response['patron'])
        patron = int(round(patron, 0)) * 1000
        funciones.Set_parametro("patron", patron)
        print "Patron actualizado:", patron/1000, "Kg"
    except Exception, e:
        print "ERROR ACTUALIZANDO PATRON: " + repr(e)
        patron = float(funciones.Get_parametro("patron"))

    try:
        LCD.lcd_string("Sincronizando...", 2, 2)
        LCD.lcd_string("Rango", 2, 2)
        #### Actualizacion de rango
        response = funciones.Web_conex("rango", estomaId, timeout=8, intentos=3)
        rango = float(response['rango'])
        print "Rango actualizado: ", rango
        funciones.Set_parametro("rango", rango)
    except Exception, e:
        print "ERROR ACTUALIZANDO RANGO: " + repr(e)
        rango = float(funciones.Get_parametro("rango"))

    try:
        #### Actualizacion de subida de datos
        response = funciones.Web_conex("subir_datos", estomaId, timeout=8, intentos=3)
        print response
        subir = int(response['subir_datos'])
        print "subida de datos actualizado: ", subir
        funciones.Set_parametro("subir_datos", subir)
    except Exception, e:
        print "ERROR ACTUALIZANDO SUBIDA DE DATOS: " + repr(e)

    try:
        #### Actualizacion de embarques
        LCD.lcd_string("Sincronizando...", 2, 2)
        LCD.lcd_string("Embarques", 2, 2)
        print funciones.Actualizar_embarques()
    except Exception, e:
        print "ERROR ACTUALIZANDO EMBARQUES: " + repr(e)

    recs, tamano = funciones.Get_embarques()

else:
    lb = int(funciones.Get_parametro("lb"))
    tara = float(funciones.Get_parametro("tara"))
    patron = float(funciones.Get_parametro("patron"))
    rango = float(funciones.Get_parametro("rango"))
    recs, tamano = funciones.Get_embarques()
    print "No se actualizaron parametros"


def init_embarques():
    nombre = str(recs[posicion][2]) + "-" + str(recs[posicion][3])
    LCD.lcd_string(nombre, 1, 1)
    minimo = recs[posicion][4]
    unidad = "Kg"
    if lb == 1:
        minimo = float(minimo) * factor
        unidad = "Lb"
        maximo = minimo + (rango * factor)
    else:
        maximo = minimo + rango
    pesos = str(round(minimo, 2)) + " - " + str(round(maximo, 2)) + " " + unidad
    if minimo < 5:
        pesos = str(round(minimo, 2)) + " " + unidad
    LCD.lcd_string(pesos, 2, 1)



##############       Funciones de operacion       ##################
def botonup(botonUp):
    global embarque
    global posicion
    global tamano
    global estado
    global calibracion
    global nombre
    global pesos
    global peso
    global patron
    global alerta
    global auxCont
    global recs
    while (True):
        time.sleep(0.1)
        if (GPIO.input(botonUp) == 0 and estado == 1):
            if (GPIO.input(botonDown)==0):
                print "+"
                auxCont += 1
            if auxCont == 2:
                auxCont = 0
                funciones.Set_parametro("calibrar", 1)
                os.execv(sys.executable, ['python'] + sys.argv)
            tamanoAnt = tamano
            recs, tamano = funciones.Get_embarques()
            difTamano = abs(tamanoAnt - tamano)
            if difTamano > 0:
                posicion = 0
            else:
                posicion += 1
            # print "Pos:" , posicion
            if (posicion == tamano) or (posicion > tamano):
                posicion = 0
            LCD.lcd_init()
            LCD.lcd_fondo(0, 0, 0)
            LCD.lcd_fondo(100, 100, 100)
            nombre = str(recs[posicion][2]) + "-" + str(recs[posicion][3])
            LCD.lcd_string(nombre, 1, 1)
            minimo = recs[posicion][4]
            unidad = "Kg"
            if lb == 1:
                minimo = float(minimo) * factor
                unidad = "Lb"
                maximo = minimo + (rango * factor)
            else:
                maximo = minimo + rango
            pesos = str(round(minimo, 2)) + " - " + str(round(maximo, 2)) + " " + unidad
            if minimo < 5:
                pesos = str(round(minimo, 2)) + " " + unidad
            LCD.lcd_string(pesos, 2, 1)
        elif (GPIO.input(botonUp) == 0 and estado == 0):
            calibracion = -1
        elif (GPIO.input(botonUp) == 0 and estado == 2):
            calibracion = 0
        elif (GPIO.input(botonUp) == 0 and estado == 3):
            calibracion = 3
        elif (GPIO.input(botonUp) == 0 and estado == 4):
            if peso > 1:
                alerta = True
                print "Retirar patron"
                time.sleep(0.5)
                LCD.lcd_init()
                LCD.lcd_fondo(0, 0, 0)
                LCD.lcd_fondo(100, 100, 100)
                LCD.lcd_clean()
                a1 = "Retirar patron"
                a2 = "para continuar"
                LCD.lcd_string(str(a1), 1, 1)
                LCD.lcd_string(str(a2), 2, 1)
                time.sleep(1.5)
                LCD.lcd_init()
                LCD.lcd_fondo(0, 0, 0)
                LCD.lcd_fondo(100, 100, 100)
                LCD.lcd_clean()
                alerta = False
            else:
                print "Inicio de pesaje"
                calibracion = -1

        elif (GPIO.input(botonUp) == 0 and estado == 5):
            if peso > 1:
                alerta = True
                print "Retirar patron"
                time.sleep(0.5)
                LCD.lcd_init()
                LCD.lcd_fondo(0, 0, 0)
                LCD.lcd_fondo(100, 100, 100)
                LCD.lcd_clean()
                a1 = "Retirar patron"
                a2 = "para continuar"
                LCD.lcd_string(str(a1), 1, 1)
                LCD.lcd_string(str(a2), 2, 1)
                time.sleep(1.5)
                LCD.lcd_init()
                LCD.lcd_fondo(0, 0, 0)
                LCD.lcd_fondo(100, 100, 100)
                LCD.lcd_clean()
                alerta = False
            else:
                print "Inicio de pesaje"
                calibracion = -1
        else:
            break


def botondown(botonDown):
    global embarque
    global posicion
    global tamano
    global estado
    global calibracion
    global nombre
    global pesos
    global recs
    while (True):
        time.sleep(0.1)
        if (GPIO.input(botonDown) == 0 and estado == 1):
            if (GPIO.input(botonUp) == 0):
                os.execv(sys.executable, ['python'] + sys.argv)
            tamanoAnt = tamano
            recs, tamano = funciones.Get_embarques()
            difTamano = abs(tamanoAnt - tamano)
            if difTamano > 0:
                posicion = 0
            else:
                posicion -= 1
            if (posicion < 0):
                posicion = tamano - 1
            LCD.lcd_init()
            LCD.lcd_fondo(100, 100, 100)
            nombre = str(recs[posicion][2]) + "-" + str(recs[posicion][3])
            LCD.lcd_string(nombre, 1, 1)
            minimo = recs[posicion][4]
            unidad = "Kg"
            if lb == 1:
                minimo = float(minimo) * factor
                unidad = "Lb"
                maximo = minimo + (rango * factor)
            else:
                maximo = minimo + rango
            pesos = str(round(minimo, 2)) + " - " + str(round(maximo, 2)) + " " + unidad
            if minimo < 5:
                pesos = str(round(minimo, 2)) + " " + unidad
            LCD.lcd_string(pesos, 2, 1)
        elif (GPIO.input(botonDown) == 0 and estado == 0):
            calibracion = 1
        elif (GPIO.input(botonDown) == 0 and estado == 2):
            calibracion = 2
        else:
            break


GPIO.add_event_detect(botonUp, GPIO.BOTH, callback=botonup, bouncetime=200)
GPIO.add_event_detect(botonDown, GPIO.BOTH, callback=botondown, bouncetime=200)


def set_estado(state):
    global estado
    global calibracion
    estado = state
    if estado == 4:
        calibracion = 4
    # print "Estado actualizado:", estado

def set_calibracion(calib):
    global calibracion
    calibracion = calib

def get_calibracion():
    global calibracion
    return calibracion


def Get_embarque():
    # global posicion
    # print "Pos:",posicion
    valores = (recs[posicion][1], recs[posicion][4])
    return valores


def get_lb():
    return lb


#### Funcion para obtener el valor del rango
def get_rango():
    rango = funciones.Get_parametro("rango")
    print "Rango entregado"
    return float(rango)


def get_patron():
    patron = funciones.Get_parametro("patron")
    print "Patron entregado"
    return float(patron)


def get_tara():
    tara = funciones.Get_parametro("tara")
    print "Tara entregada"
    return float(tara)


def Get_ratio_hx():
    try:
        ratio = float(funciones.Get_parametro("ratio"))
        return ratio
    except Exception, e:
        print "Falla en lectura de ratio", repr(e)
        return 0

def Set_peso(pes):
    global peso
    peso = pes

def Get_alerta():
    global alerta
    return alerta
