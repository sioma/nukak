# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#                Libreria de HX711 para Nukak                       #
#####################################################################
######               Importacion de librerias                 #######
from hx711 import HX711
import funciones
import LCD
import time
import credentials


parametros = credentials.Get_Parametros_Filtro()
diferenciaRatio = 2

class HXNUKAK:
    #### Funcion de inicializacion de HX711
    def __init__(self, dout=10, sck=9, debug=False, tara=0, ratio=0, patron=20.0):
        self.resolucion = parametros[7]
        try:
            for i in range(3):
                self.hx = HX711(dout_pin=dout, pd_sck_pin=sck)
                self.hx.set_debug_mode(debug)
                inicio = self.hx.reset()
                if inicio:
                    break
            self.hx.zero(times=10)
            ratio = float(funciones.Get_parametro("ratio"))
            self.hx.set_scale_ratio(scale_ratio=ratio)
            self.tara = tara
            self.patron = patron
            print "HX711 Iniciado con exito"
        except Exception, e:
            print "Error iniciando HX711: " + repr(e)


    #####################################################################
    #####################################################################
    ######                        Funciones                       #######

    #### Obtener lecturas de peso
    def Get_lectura(self, intentosMax = 3):
        lectura = self.hx.get_weight_mean()
        intentos = 0
        while not lectura and intentos <= intentosMax:
            lectura = self.hx.get_weight_mean()
            intentos += 1
        if not lectura:
            return False
        else:
            return (lectura/1000)-self.tara

    #### Funcion de calibracion de Nukak
    def Calibrar_nukak(self):
        data = self.hx.get_data_mean(times=10)

        if data != False:
            print('Mean value from HX711 subtracted by offset: ' + str(data))
            known_weight_grams = self.patron*1000

            try:
                value = float(known_weight_grams)
                print(str(value) + ' g')
            except ValueError:
                print('Expected integer or float and I have got: ' \
                      + str(known_weight_grams))
            ratio = data / value  # calculate the ratio for channel A and gain 64
            print 'Ratio: ', ratio

            if ratio < 0:
                ratio = 2

            self.hx.set_scale_ratio(scale_ratio=ratio)  # set ratio for current channel
            pesoValidacion = round(self.hx.get_weight_mean(10)/1000, 2)

            print "Peso de calculado: ", pesoValidacion, "Patron: ", self.patron
            print "Diferencia: ", abs(pesoValidacion-self.patron)
            if abs(pesoValidacion - self.patron) > self.resolucion:
                LCD.lcd_string("ERROR DE", 1, 2)
                LCD.lcd_string("CALIBRACION", 2, 2)
                time.sleep(2)
                return 2, 2

            funciones.Set_parametro("ratio", ratio)
            print('Ratio is set.')
            fecha = funciones.Actualizar_hora()
            #fecha = '2019-01-18 15:57:25'
            print fecha
            funciones.Insert_Validacion(0, fecha, self.patron / 1000, data, ratio, pesoValidacion)
            funciones.Set_parametro("calibrar", 0)
            calibracion = 4
            return calibracion, 4
        else:
            raise ValueError('Cannot calculate mean value. Try debug mode.')


