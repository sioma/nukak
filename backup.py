# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#           Codigo para bakcup de bd y datos de log                 #
#####################################################################
######               Importacion de librerias                 #######
import funciones
from subprocess import PIPE, Popen

periodoBackupDb = 30
periodoBackupLog = 1

def cmdline(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]


######  Inicializacion
funciones.Conect_db_backup()
fecha = funciones.Actualizar_hora(dia=1)
###### Backup de base de datos
lastBd = funciones.Get_Last_Backup('db')
if lastBd is None or (fecha - lastBd).days >= periodoBackupDb and funciones.Get_Subido():
    print cmdline("sudo tar --create --gzip --file='/home/pi/datos/db.tgz' '/home/pi/datos/db'")
    print cmdline("sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -c '\copy bandejas to '/home/pi/datos/db/racimitos.csv' with csv'")
    funciones.Insert_Backup(fecha, 'db')
    print cmdline("sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -c 'drop table bandejas;'")
else:
    print "Backup de DB se encuentra actualizado"

lastLog = funciones.Get_Last_Backup('log')

if lastLog is None or (fecha - lastLog).days >= periodoBackupLog:
    print cmdline("sudo tar --create --gzip --file='/home/pi/datos/logData.tgz' '/home/pi/datos/logData'")
    funciones.Insert_Backup(fecha, 'log')
    print cmdline("sudo rm /home/pi/datos/logData/ConFiltro.txt /home/pi/datos/logData/SinFiltro.txt")
else:
    print "Backup de logs se encuentra actualizado"

