# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#           Codigo para captacion de bandejas Sigma
#####################################################################
###############         Importacion de librerias   ##################
import time, datetime,serial,sys,glob
import logging as log
import threading
import psycopg2 
import psycopg2.extensions
import psycopg2.extras
import json
import urllib2, urllib
import RPi.GPIO as GPIO
import socket
import multiprocessing as mp
import credentials
import requests
import numpy as np
#import bat
import seg

# Setear GPIO en configuracion Broadcom y definir los pines del RGB 
RUNNING = True
GPIO.setmode(GPIO.BCM)
red = 17
green = 18
blue = 27
gnd=22
buzzer=24
GPIO.setup(buzzer,GPIO.OUT)
GPIO.output(buzzer,0)
class GetPeso(mp.Process):
    
    def __init__(self):
        mp.Process.__init__(self)
        #Inicializacion de las variables para lectura de pesos
        self.lectura=0
	self.ultima_lectura=0
        self.subida=0
        self.subida_ant=0
        self.i=0
        self.max_peso=60 #Define el peso maximo que recibira el siste$
        self.upper_range=1  #Por defecto debe estar en 7
        self.down_range=1   #Por defecto debe estar en 8
        self.data_avg=0.2   #Por defecto debe estar en 0.2
	self.len=20          #Por Definir
	self.vec=[]
	self.sound=0
	self.ini=True
	self.alfa=0.1054
	self.tara=0
	self.ultima1_lecturaNoFil=0
	self.lecturaNoFil=0
	#for i in range(0,self.len):
		#self.vec.append(0)

    def run(self):
        while True:
            try:
		#print "Lectura: ",hx.get_weight_mean(1)/1000
		if np.std(self.vec)>0.02 or self.ini==True :
			self.sound=0
			self.ultima_lectura=self.lectura
			self.ultima2_lecturaNoFil=self.ultima1_lecturaNoFil
			self.ultima1_lecturaNoFil=self.lecturaNoFil
			self.lecturaNoFil=hx.get_weight_mean(1)/1000
			self.dif1=(self.ultima2_lecturaNoFil-self.ultima1_lecturaNoFil)
			self.dif=self.ultima1_lecturaNoFil-self.lecturaNoFil
			if self.dif1 < -1 and self.dif > 1:
				self.ultima1_lecturaNoFil=self.ultima2_lecturaNoFil
			if self.ultima1_lecturaNoFil<0:
				self.ultima1_lecturaNoFil=self.ultima_lectura
			self.lectura=(self.alfa*(self.ultima1_lecturaNoFil)+((1-self.alfa)*self.ultima_lectura))-self.tara
			print self.lectura
			if len(self.vec)<self.len:
				self.vec.append(self.lectura)
			elif len(self.vec)==self.len:
				self.ini=False
				self.vec=self.vec[1:]
				self.vec.append(self.lectura)
			seg.show(round(np.average(self.vec),2))
			self.a=open("lecturasNoFil3.txt","a")
			self.a.write(str(self.ultima1_lecturaNoFil)+'\n')
                        self.a.close()
			self.f=open("lecturasFiltradas3.txt","a")
                        self.f.write(str(self.lectura)+'\n')
                        self.f.close()
		if np.std(self.vec)<0.02:
			if self.sound==0 and np.average(self.vec)>1:
				GPIO.output(buzzer,1)
				time.sleep(0.03)
				GPIO.output(buzzer,0)
				self.sound=1
			self.ultima_lectura=self.lectura
                        self.ultima2_lecturaNoFil=self.ultima1_lecturaNoFil
                        self.ultima1_lecturaNoFil=self.lecturaNoFil
                        self.lecturaNoFil=hx.get_weight_mean(1)/1000
                        self.dif1=(self.ultima2_lecturaNoFil-self.ultima1_lecturaNoFil)
                        self.dif=self.ultima1_lecturaNoFil-self.lecturaNoFil 
                        if self.dif1 < -1 and self.dif > 1:
                                self.ultima1_lecturaNoFil=self.ultima2_lecturaNoFil
                        if self.ultima1_lecturaNoFil<0:
                                self.ultima1_lecturaNoFil=self.ultima_lectura
                        self.lectura=(self.alfa*(self.ultima1_lecturaNoFil)+((1-self.alfa)*self.ultima_lectura))-self.tara
			print self.lectura
			if len(self.vec)<self.len:
                                self.vec.append(self.lectura)
                        elif len(self.vec)==self.len:
                                self.vec=self.vec[1:]
                                self.vec.append(self.lectura)
                        seg.show(round(np.average(self.vec),2))
			self.a=open("lecturasNoFil3.txt","a")
                        self.a.write(str(self.ultima1_lecturaNoFil)+'\n')
                        self.a.close()
			self.f=open("lecturasFiltradas3.txt","a")
                        self.f.write(str(self.lectura)+'\n')
                        self.f.close()
		print "Desviacion",np.std(self.vec)
                self.subida_ant=self.subida
                if self.lectura>self.upper_range and self.lectura<self.max_peso:
                        self.subida=1
                elif self.lectura<self.down_range:
                        self.subida=0
                if self.subida_ant==1 and self.subida==0:
                        print "Dato capturado"
                        print self.vec
			self.vec_peso=self.vec[:self.len-8]
			print "Datos usados", self.vec_peso
                        self.peso=round(np.average(self.vec_peso),2)
                        print "Peso obtenido: ",self.peso
                        #UpdateDb(self.peso)
                        GPIO.output(buzzer,1)
                        time.sleep(0.03)
                        GPIO.output(buzzer,0)
            except IOError, e:
                print "ERROR: Caught exception: " + repr(e)


from hx711 import HX711
listo=0
while listo==0:
 try:
        hx=HX711(dout_pin=10,pd_sck_pin=9)
        hx.set_debug_mode(False)
        hx.reset()
        hx.zero(times=10)
        file=open("ratio.txt","r")
        ratio=float(file.read())
        hx.set_scale_ratio(scale_ratio=ratio)
        print("Ratio actualizado")
        listo=1
        hx.set_debug_mode(False)

 except Exception,e:
        print "Error iniciando HX711, reintentando... "  + repr(e)

### Se crean los hilos
SerialBascula = GetPeso()
### Se inicia el hilo de lectura serial
SerialBascula.daemon=False
SerialBascula.start()
