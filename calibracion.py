from hx711 import HX711
import RPi.GPIO as GPIO
import time

try:
	hx = HX711(dout_pin=10, pd_sck_pin=9)
	result = hx.reset()
	hx.zero(times=10)
	#hx.set_debug_mode(True)
	if result:                      # you can check if the reset was successful
                print('Ready')
        else:
                print('not ready')
	raw_input('Poner peso conocido y despues pulsar Enter')
	data = hx.get_data_mean(times=10)
        if data != False:
        	print('Mean value from HX711 subtracted by offset: ' + str(data))
                known_weight_grams = raw_input('Ingrese el valor de peso conocido en Kg y pulse Enter: ')
                try:
                	value = float(known_weight_grams)
                        print(str(value) + ' Kg')
                except ValueError:
                        print('Expected integer or float and I have got: '\
                                                + str(known_weight_grams))

                ratio = data / value    # calculate the ratio for channel A and gain 64
                print 'Ratio: '	,ratio
                rfile = open("ratio.txt","w")
                rfile.write(str(ratio))
                rfile.close()
                hx.set_scale_ratio(scale_ratio=ratio)   # set ratio for current channel
                print('Ratio is set.')
	else:
                raise ValueError('Cannot calculate mean value. Try debug mode.')
except (KeyboardInterrupt, SystemExit):
	print ('Ending...')

finally:
	GPIO.cleanup()
