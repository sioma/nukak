import time
from Adafruit_LED_Backpack import SevenSegment

display = SevenSegment.SevenSegment()

try:
    display.begin()
    display.clear()
    display.print_float(0.00)
    display.write_display()
except Exception, e:
    print "ERROR INICIANDO 7 SEGMENTOS: " + repr(e)


def show(peso):
    display.clear()
    display.print_float(peso)
    display.set_colon(False)
    display.write_display()


def Show_peso(peso, lb=False):
    global display
    if lb:
        peso = peso*2.205
    try:
        show(peso)
    except Exception, e:
        print "ERROR COMUNICACION CON 7 SEGMENTOS: ", repr(e)
        print "Reiniciando 7 segmentos..."
        try:
            display = SevenSegment.SevenSegment()
            display.begin()
        except Exception, e:
            print "ERROR REINICIANLIZANDO 7 segmentos", repr(e)


def nulo():
    display.clear()
    display.set_digit(0, " ")
    display.set_digit(1, " ")
    display.set_digit(2, " ")
    display.set_digit(3, " ")
    display.set_colon(False)
    display.write_display()

def full():
    display.clear()
    display.print_float(88.88)
    display.set_colon(False)
    display.write_display()