# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#           Funciones para captacion de racimos Sigma
#####################################################################
######               Importacion de librerias                 #######
import time, datetime
import psycopg2, psycopg2.extensions, psycopg2.extras
import json, urllib, requests
import RPi.GPIO as GPIO
import credentials
import socket
from hx711 import HX711
import operator
import LCD
import numpy as np
from operator import itemgetter
from math import floor
from subprocess import PIPE, Popen
####### Localizacion de archivos


####### Variables de identificacion
estomaInfo = credentials.Get_Estoma_Info()
estomaId = estomaInfo[0]


#####################################################################
#####################################################################
######                        Funciones                       #######
########## Funcion de inicializacion de RGB
def Init_RGB(r, g, b, gnd):
    global RED,GREEN,BLUE
    # Setear GPIO en configuracion Broadcom y definir los pines del RGB
    GPIO.setmode(GPIO.BCM)
    red = r
    green = g
    blue = b
    gnd = gnd
    # Setear el modo de operacion de los pines
    GPIO.setwarnings(False)
    GPIO.setup(red, GPIO.OUT)
    GPIO.setup(green, GPIO.OUT)
    GPIO.setup(blue, GPIO.OUT)
    GPIO.setup(gnd, GPIO.OUT)
    GPIO.output(gnd, 0)
    Freq = 100  # Hz
    # Setup all the LED colors with an initial
    # duty cycle of 0 which is off
    RED = GPIO.PWM(red, Freq)
    RED.start(0)
    GREEN = GPIO.PWM(green, Freq)
    GREEN.start(0)
    BLUE = GPIO.PWM(blue, Freq)
    BLUE.start(0)


############## Funcion para el manejo de RGB
def RGB(color, blinks=1, sleep=0.2):
    ###Posibles colores: Azul, Rojo, Verde, Blanco, Amarillo, Morado
    global R, G, B
    R = 0
    G = 0
    B = 0
    if color == "Azul":
        R = 0
        G = 0
        B = 100
    elif color == "Rojo":
        R = 100
        G = 0
        B = 0
    elif color == "Verde":
        R = 0
        G = 100
        B = 0
    elif color == "Blanco":
        R = 100
        G = 100
        B = 100
    elif color == "Amarillo":
        R = 100
        G = 100
        B = 0
    elif color == "Morado":
        R = 100
        G = 0
        B = 100
    else:
        print "ERROR: Color no valido"
    if blinks == 0:
        RED.ChangeDutyCycle(R)
        GREEN.ChangeDutyCycle(G)
        BLUE.ChangeDutyCycle(B)
    else:
        for i in range(0, blinks):
            RED.ChangeDutyCycle(R)
            GREEN.ChangeDutyCycle(G)
            BLUE.ChangeDutyCycle(B)
            time.sleep(sleep)
            RED.ChangeDutyCycle(0)
            GREEN.ChangeDutyCycle(0)
            BLUE.ChangeDutyCycle(0)
            time.sleep(sleep)


################# Funcion para la conexion a la base de datos
def Conect_db():
    try:
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')  # Conexión bases de datos
        cursor = conectar.cursor(cursor_factory=psycopg2.extras.DictCursor)  # Varibale Cursor con configuracion de generar el dict
        cursor.execute(
            "CREATE TABLE IF NOT EXISTS bandejas(bandeja_id SERIAL PRIMARY KEY, fecha TEXT, peso TEXT, sobrepeso float4,embarque_id TEXT, tara TEXT, estoma_nombre TEXT, subido INT)")  # Ejecución sentencia SQL
        conectar.commit()
        print'Conexion con BD inicial RDY'
    except Exception, e:
        print "ERROR: " + repr(e)


################# Funcion de actualizacion de la hora
def Actualizar_hora(dia=0):

    if dia == 1:
        Date = datetime.datetime.now()
        Date = Date.date()
        return Date
    else:
        Hours = datetime.datetime.now().strftime('%H:%M:%S')
        Date = datetime.datetime.now().strftime('%Y-%m-%d')
        fecha_peso = Date + " " + Hours
        return fecha_peso


###### Funcion de creacion de cursor
def Create_cursor(json=False):
    if json:
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')
        conectar.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_REPEATABLE_READ)
        cursor = conectar.cursor(cursor_factory=psycopg2.extras.DictCursor)
    else:
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')
        conectar.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_REPEATABLE_READ)
        cursor = conectar.cursor()
    return cursor, conectar


### Funcion que realiza la actualizacion en la base de datos local
def Update_db(peso, tara, embarqueId, sobrepeso):
    print "Actualizando db interna..."
    peso = peso
    print "Peso: ", peso
    if peso == "":
        peso = 0
    fechaPeso = Actualizar_hora()  # Actualizar la fecha a almacenar
    cursor, conectar = Create_cursor()
    try:
        # se realiza la sentencia SQL registrando la bandera de subido en 0
        cursor.execute("insert into bandejas (fecha, peso, sobrepeso, embarque_id, tara, estoma_nombre, subido) values (%s, %s, %s, %s, %s, %s, %s)",(fechaPeso, peso, sobrepeso, embarqueId, tara, estomaId, 0))
        print "BD actualizada"
    except psycopg2.Error, e:
        print "error al actualizar la bd interna"

    conectar.commit()


### Funcion que obtiene los datos de la bd interna en un dictionary y los almacena en un JSON para envio a la web
def Json_data():
    global bandejaId
    try:
        cursor, conectar = Create_cursor(json=True)
        conectar.commit()
        # Validar que los datos a subir aun no se han registrado en la web para no tener datos duplicados, verificando que la bandera subidos este en 0
        cursor.execute("SELECT * FROM bandejas WHERE subido='0' ORDER BY fecha ASC LIMIT 10")
        recs = cursor.fetchall()
        rows = [dict(rec) for rec in recs]
        # Validar que la lista de dictionaries no este vacia
        if (len(rows) > 0):
            print "Generando Json..."
            #print 'Valores seleccionados:'
            #print rows
            lastdict = rows[-1]
            bandejaId = lastdict.get('bandeja_id')
            rows_json = json.dumps(rows)
            return rows_json
        else:
            return 0
    except Exception, e:
        print "ERROR OBTENIENDO JSON: " + repr(e)
        return 0


############# Funcion para realizar la conexion a la web
def Web_conex(tabla, datos, timeout=6, intentos=1):
    print "Tabla destino: ", tabla
    print "Datos a enviar: ", datos
    SiomappCredentials = credentials.Get_Siomapp_Credentials()
    mydata = [('s', SiomappCredentials[0]), ('m', tabla), ('d', datos)]
    mydata = urllib.urlencode(mydata)
    path = 'http://banapeso.siomapp.com/bandejas/subir.php'
    header = {"Content-type": "application/x-www-form-urlencoded"}
    ok = False
    for i in range(0, intentos):
        i += 1
        if not ok:
            try:
                page = requests.post(path, data=mydata, headers=header, timeout=timeout)
                return page.json()
                ok = True
            except Exception, e:
                print "ERROR EN LA CONEXION A SIOMA: " + repr(e)
                return 0
        else:
            break


############# Funcion para realizar la conexion a la web
def Web_noti(subject, datos, timeout=6, intentos=1):
    print "Subject: ", subject
    print "Datos a enviar: ", datos
    SiomappCredentials = credentials.Get_Siomapp_Credentials()
    mydata = [('s', SiomappCredentials[0]), ('subject', subject), ('body', datos)]
    mydata = urllib.urlencode(mydata)
    path = 'https://filtro.siomapp.com/1/correoAlertaStaff'
    header = {"Content-type": "application/x-www-form-urlencoded", "Authorization": "gS8Jdz24bJgevS9loGuiCW9R2IJXLcpdme8nQ8b88No"}
    ok = False
    for i in range(0, intentos):
        i += 1
        if not ok:
            try:
                page = requests.post(path, data=mydata, headers=header, timeout=timeout)
                return page.json()
                ok = True
            except Exception, e:
                print "ERROR EN LA CONEXION A SIOMA: " + repr(e)
                return 0
        else:
            break

### Funcion que realiza la conexion con el servidor web y realiza el envio del JSON
def Sync():
    global bandejaId
    base = Json_data()
    try:
        if base != 0:
            if base != "[]":
                response = Web_conex("bandejas", base)
                if response !=0:
                    print response
                    response = sorted(response.items(), key=operator.itemgetter(0))
                    response = dict((key, value) for key, value in response)
                    keys = []
                    flag = 0
                    for key in response.keys():
                        if key == 'error':
                            flag = 1
                        keys.append(key)
                    if flag == 1:
                        print "Error de request: ", response['error']
                    else:
                        cursor, conectar = Create_cursor()
                        for i in range(0, len(response)):
                            # Actualizar la bandera de subido a los datos que se han ingresado a la web
                            cursor.execute("UPDATE bandejas SET subido=%s WHERE subido='0' and bandeja_id <= (%s) and bandeja_id = %s", (response[keys[i]], bandejaId, keys[i],))
                            conectar.commit()
    except Exception, e:
        print "ERROR: " + repr(e)


#### Funcion para la validacion de conexion a internet
def Test_online():
    try:
        socket.setdefaulttimeout(8)
        socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect(("8.8.8.8", 53))
        print 'System Online'
        return 1
    except Exception, e:
        print 'System Offline'
        print "ERROR TESTING INTERNET: " + repr(e)
        return 0


#### Funcion para hacer shift a vectores
def Shift(vec, dato):
    vec = vec[1:]
    vec.append(dato)
    return vec


#### Funcion de validacion de sobrepeso
def Validar_sobrepeso(peso, embarque, rango, downRange, rangoValidacion = False):
    if (embarque == 0):
        dif = 0
    else:
        dif = (float(peso) - float(embarque))
    # print "Diferencia: " + str(dif)
    if peso < downRange:
        LCD.lcd_fondo(100, 100, 100)
    elif float(peso) < float(embarque):
        LCD.lcd_fondo(100, 0, 0)
    # print "Bajopeso"
    elif dif > rango:
        LCD.lcd_fondo(100, 10, 0)
    # print "Sobrepeso"
    else:
        # print "Correcto"
        LCD.lcd_fondo(0, 100, 0)
    return dif


#### Funcion para ordenamiento
def Get_Key(item):
    return item[1]


#### Funcion para determinar la estabilidad
def Estable (vecLecturas, estabilidad, tam):
    promedio=np.average(vecLecturas)
    vectorDif=[]
    for i in range(0,len(vecLecturas)-1):
        vectorDif.append(abs(vecLecturas[i]-promedio))
    if np.average(vectorDif) < estabilidad and len(vecLecturas) == tam:
        return True
    else:
        return False


#### Funcion para validar un rango valido
def Valildacion_Rango (vec, embarque, rango, tara):
    if embarque == 0:
        return True
    elif abs(np.average(vec)-tara-embarque) > rango:
        return False
    else:
        return True


#### Funcion para obtener el valor del peso
def Get_Peso(vec, porColas):
    promedio = np.average(vec)
    vectorDif = []
    for i in range(0, len(vec) ):
        vectorDif.append([vec[i], abs(vec[i] - promedio)])
    vecOrdenado = sorted(vectorDif, key=Get_Key)
    tam = int(floor(len(vecOrdenado)*porColas))
    vecOrdenado = vecOrdenado[:tam]
    vecPesos = [row[0] for row in vecOrdenado]
    peso = np.average(vecPesos)
    return peso, vecPesos


#### Funcion para crear la tabla de embarques
def Conect_Db_Embarques():
    try:
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')  # Conexión bases de datos
        cursor = conectar.cursor(cursor_factory=psycopg2.extras.DictCursor)  # Varibale Cursor con configuracion de generar el dict
        cursor.execute("CREATE TABLE IF NOT EXISTS embarques(id SERIAL PRIMARY KEY, embarque_id TEXT,codigo TEXT, nombre TEXT, peso_kg FLOAT4, peso_lb FLOAT4, activo INT DEFAULT 0, updated_at TIMESTAMP, created_at TIMESTAMP)")
        conectar.commit()
        print'Conexion con BD inicial RDY'
    except Exception, e:
        print "ERROR: " + repr(e)


#### Funcion para borrar la tabla
def Borrar_Embarques():
    conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')
    cursor = conectar.cursor(cursor_factory=psycopg2.extras.DictCursor)
    cursor.execute("DROP TABLE embarques")
    conectar.commit()


def Save_Embarques_Activos():
    curs, conn = Create_cursor(json=True)
    curs.execute("SELECT codigo FROM embarques WHERE activo=1 ORDER BY codigo ASC")
    recs = curs.fetchall()
    conn.commit()
    return recs


def Activar_Embarque(embarques):
    curs, conn = Create_cursor()
    for codigo in embarques:
        code = codigo[0]
        curs.execute("UPDATE embarques SET activo=1 WHERE codigo=%s", (code,))
    conn.commit()

#### Funcion para guardar datos en archivo
def Actualiar_archivo(path, dato):
    f = open(path, "w")
    fecha = Actualizar_hora()
    f.write(fecha + "\n")
    f.write(str(dato))
    f.close()


#### Funcion para actualizar embarques en base de datos local
def Actualizar_embarques():
    response = Web_conex("embarques", estomaId, timeout=4, intentos=3)
    print response
    temp = []
    dictlist = []
    for key, value in response.iteritems():
        temp = [key, value]
        dictlist.append(temp)
    list = dictlist[0][1]
    print "Cantidad de tipos de embarque registrados: " + str(len(list))
    if len(list) < 1:
        curs, conn = Create_cursor()
        curs.execute("UPDATE embarques SET activo=1")
        conn.commit()
        print "ERROR, NINGUN EMBARQUE ACTIVADO"
    else:
        activos = Save_Embarques_Activos()
        print "Embarques activos: ", activos
        Borrar_Embarques()
        Conect_Db_Embarques()
        curs, conn = Create_cursor()
        for i in list:
            curs.execute(
                'insert into embarques(embarque_id, codigo, nombre, peso_kg, peso_lb) values (%s, %s, %s, %s, %s)',
                (i['embarque_id'], i['codigo'], i['nombre'], i['peso_kg'], i['peso_lb']))
            conn.commit()
        Activar_Embarque(activos)
        return "Base de datos de embarques actualizada"


#### Funcion para obtener los embarques almacenados localmente
def Get_embarques():
    curs, conn = Create_cursor(json=True)
    curs.execute("SELECT * FROM embarques WHERE activo=1 ORDER BY codigo ASC")
    recs = curs.fetchall()
    if len(recs) < 1:
        curs.execute("SELECT * FROM embarques ORDER BY codigo ASC")
        recs = curs.fetchall()
    conn.commit()
    tamano = len(recs)
    return recs, tamano


#### Funcion para log de datos
def Log_datos(dato, archivo):
    a = open(archivo, "a")
    a.write(str(dato)+'\n')
    a.close()
    
    
################# Funcion para la creacion de la tabla de parametros
def Conect_db_parametros():
    try:
        fecha = Actualizar_hora()
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')  # Conexión bases de datos
        cursor = conectar.cursor(cursor_factory=psycopg2.extras.DictCursor)  # Varibale Cursor con configuracion de generar el dict
        cursor.execute("CREATE TABLE IF NOT EXISTS parametros(parametro_id SERIAL PRIMARY KEY, parametro TEXT UNIQUE, fecha TEXT, valor TEXT)")  # Ejecución sentencia SQL
        conectar.commit()
        print'Conexion con tabla de parametros'
    except Exception, e:
        print "ERROR: " + repr(e)
        

def Get_parametro(parametro):
    try:
        cursor, conectar = Create_cursor(json=True)
        conectar.commit()
        # Validar que los datos a subir aun no se han registrado en la web para no tener datos duplicados, verificando que la bandera subidos este en 0
        cursor.execute("SELECT * FROM parametros")
        recs = cursor.fetchall()
        rows = [dict(rec) for rec in recs]
        existe = False
        for i in range(0, len(rows)):
            if rows[i]['parametro'] == parametro:
                existe = True
                #print "El parametro ", parametro, "no se sincronizo, ultima fecha de actualizacion: ", rows[i]['fecha']
                return rows[i]['valor']
        if not existe:
            print "El parametro ", parametro, "no existe"
            return 1
    except Exception, e:
        return 0
        print "ERROR OBTENIENDO PARAMETROS: " + repr(e)


def Set_parametro(parametro, valor):
    fecha = Actualizar_hora()  # Actualizar la fecha a almacenar
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT * FROM parametros")
    recs = cursor.fetchall()
    rows = [dict(rec) for rec in recs]
    existe = False
    print parametro
    if parametro == "ratio" and valor < 1:
        valor = 2
    for i in range(0, len(rows)):
        if rows[i]['parametro'] == parametro:
            id = rows[i]['parametro_id']
            existe = True
    try:
        if existe:
            cursor.execute("update parametros set valor=%s, fecha=%s where parametro_id=%s;", (valor, fecha, id))
            print "Paramretro ", parametro, "Actualizado"
        else:
            cursor.execute("insert into parametros (parametro, fecha, valor) values (%s, %s, %s)", (parametro, fecha, valor))
            print "Paramretro ", parametro, "Actualizado"

        conectar.commit()
    except Exception, e:
        print "ERROR ACTUALIZANDO PARAMETRO ", parametro, ": ", repr(e)


################# Funcion para la conexion a la base de datos de backup
def Conect_db_backup():
    try:
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')  # Conexión bases de datos
        cursor = conectar.cursor(cursor_factory=psycopg2.extras.DictCursor)  # Varibale Cursor con configuracion de generar el dict
        cursor.execute("CREATE TABLE IF NOT EXISTS backups(backup_id SERIAL PRIMARY KEY, fecha DATE, tipo TEXT)")  # Ejecución sentencia SQL
        conectar.commit()
        print'Conexion con BD backups inicial RDY'
    except Exception, e:
        print "ERROR: " + repr(e)

def Insert_Backup(fecha, tipo):
    fecha = fecha
    cursor, conectar = Create_cursor()
    try:
        cursor.execute("insert into backups (fecha, tipo) values (%s, %s)", (fecha, tipo))
        print "Backup de " + tipo + " registrado "
        conectar.commit()
    except Exception, e:
        print "ERROR INSERTANDO BACKUP: ", repr(e)

def Get_Last_Backup(tipo):
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    if tipo=='db':
        cursor.execute("SELECT max(fecha) FROM backups WHERE tipo='db';")
    elif tipo=='log':
        cursor.execute("SELECT max(fecha) FROM backups WHERE tipo='log';")
    recs = cursor.fetchall()
    return recs[0][0]

def Get_Subido():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT max(fecha) FROM bandejas WHERE subido=0;")
    recs = cursor.fetchall()
    if recs[0][0] is None:
        return True
    else:
        return False


################# Funcion para la conexion a la base de datos de calibracion
def Conect_db_Calibracion():
    try:
        conectar = psycopg2.connect('dbname=estomadb user=postgres password=sioma')  # Conexión bases de datos
        cursor = conectar.cursor(cursor_factory=psycopg2.extras.DictCursor)  # Varibale Cursor con configuracion de generar el dict
        cursor.execute("CREATE TABLE IF NOT EXISTS calibracions(calibracion_id SERIAL PRIMARY KEY, tipo INT, fecha TIMESTAMP, patron INT, valor_hx FLOAT, ratio FLOAT, peso_validacion FLOAT)")  # Ejecución sentencia SQL
        conectar.commit()
        print'Conexion con BD Calibraciones inicial RDY'
    except Exception, e:
        print "ERROR: " + repr(e)


def Insert_Validacion(tipo, fecha, patron, valor_hx, ratio, peso_validacion):
    fecha = fecha
    cursor, conectar = Create_cursor()
    try:
        cursor.execute("insert into calibracions (tipo, fecha, patron, valor_hx, ratio, peso_validacion) values (%s, %s, %s, %s, %s, %s)", (tipo, fecha, patron, valor_hx, ratio, peso_validacion))
        print "Calibracion registrada"
        conectar.commit()
    except Exception, e:
        print "ERROR INSERTANDO CALIBRACION: ", repr(e)


def Get_Last_Validacion():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT max(fecha) FROM calibracions;")
    recs = cursor.fetchall()
    return recs[0][0]


def Get_Last_Ratio():
    cursor, conectar = Create_cursor(json=True)
    conectar.commit()
    cursor.execute("SELECT ratio FROM calibracions order by fecha desc limit 1;")
    recs = cursor.fetchall()
    if len(recs) > 0:
        return recs[0][0]
    else:
        return None


def Cmd_line(command):
    process = Popen(
        args=command,
        stdout=PIPE,
        shell=True
    )
    return process.communicate()[0]