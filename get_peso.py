# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#           Codigo para captacion de racimos Sigma                  #
#####################################################################
######               Importacion de librerias                 #######
import time, funciones
import numpy as np
import HxNukak as hn
import seg, LCD
from math import floor
import parametros as pm
import filtros as fil
import credentials
#####################################################################
######               Declaracion de variables                 #######
hxDebug = False
parametros = credentials.Get_Parametros_Filtro()

class GetPeso:

    def __init__(self, sensor):
        self.sensor = sensor
        self.intentosMax = 3
        self.reboot = 0
        self.lenVec = parametros[0]
        self.lenVecFilP = parametros[1]
        self.vec = []
        self.alfa = parametros[2]
        self.alfaM = parametros[3]
        self.alfaG = parametros[4]
        self.difG = parametros[5]
        self.difM = parametros[6]
        self.resolucion = parametros[7]
        self.pesoEstable = -666  # Debe ser un negativo grande en magnitud
        self.rangoValido = 2
        self.pesoMin = 7
        self.embarque = 0
        self.embarqueId = 0
        self.sobrepeso = 0
        self.rango = pm.get_rango()
        self.estabilidad = 0.5
        self.time = time.time()
        self.tiempo = 0.5
        self.colorTime = 0.5
        self.colorTimeEstable = 0.1
        self.valorColorTime = time.time()
        self.saveData = True
        self.showTime = 0.15
        self.valorShowTime = time.time()
        self.primerShow = False
        self.primerEstable = False
        self.pesoSubida = 1
        self.tara = float(funciones.Get_parametro("tara"))
        self.lb = pm.get_lb()

    def Llenar_vector(self):
        self.vec = []
        for i in range(0, self.lenVec):
            lectura = self.sensor.Get_lectura()
            if not lectura:
                LCD.lcd_clean()
                LCD.lcd_string("REINICIE", 1, 2)
                while True:
                    pass
            self.vec.append(lectura)
        print "Vector llenado"

    def Get_lectura(self):
        pesoAdd = self.sensor.Get_lectura()+self.tara
        self.vec = funciones.Shift(self.vec, pesoAdd)
        if self.saveData:
            funciones.Log_datos(self.vec[self.lenVec-1], "SinFiltroCal.txt")
        self.vec[self.lenVec - self.lenVecFilP:self.lenVec] = fil.Filtro_picos(
            self.vec[self.lenVec - self.lenVecFilP:self.lenVec])
        self.vec[self.lenVec - self.lenVecFilP:self.lenVec] = fil.Filtro_picos(
            self.vec[self.lenVec - self.lenVecFilP:self.lenVec], vg=self.difG, pd=0.7)
        self.vec[self.lenVec - self.lenVecFilP - 1] = fil.Filtro_pasa_bajas(self.vec[self.lenVec - self.lenVecFilP - 1],
                                                                            self.vec[self.lenVec - self.lenVecFilP - 2],
                                                                            self.alfa, self.alfaM, self.alfaG,
                                                                            self.difM, self.difG)
        if self.saveData:
            tFile = open('/sys/class/thermal/thermal_zone0/temp')
            temp = float(tFile.read())
            tempC = temp/1000
            funciones.Log_datos(tempC, "temp.txt")
            funciones.Log_datos(self.vec[self.lenVec - self.lenVecFilP - 1], "ConFiltroCal.txt")

    def Show_peso(self, peso):
        if self.primerShow:
            if self.primerEstable:
                self.primerShow = False
                peso = floor(peso / self.resolucion) * self.resolucion
                if peso < 1:
                    peso = -self.tara
                seg.Show_peso(peso, self.lb)
            else:
                seg.Show_peso(-10.00)
        else:
            peso = floor(peso / self.resolucion) * self.resolucion
            if peso < 1:
                peso = -self.tara
            seg.Show_peso(peso, self.lb)

    def Estable(self):
        return fil.Estable(self.vec[:self.lenVec - self.lenVecFilP], self.resolucion * self.estabilidad)

    def Validar(self):
        self.Llenar_vector()
        peso = self.Execute()
        while not self.Estable():
            peso = self.Execute()
        return peso

    def Get_peso(self):
        std = np.std(self.vec[:self.lenVec - self.lenVecFilP])
        prom = np.mean(self.vec[:self.lenVec - self.lenVecFilP])
        vMax = prom + std
        vMin = prom - std
        vecAux = []
        for i in range(0, self.lenVec - self.lenVecFilP):
            valor = self.vec[i]
            if vMin < valor < vMax:
                vecAux.append(valor)
        return np.mean(vecAux)

    def Execute(self):
        self.Get_lectura()
        peso = self.Get_peso()
        #if time.time() - self.valorShowTime > self.showTime:
        #    self.valorShowTime = time.time()
        #    self.Show_peso(peso)
        return peso