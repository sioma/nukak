# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#           Codigo para captacion de racimos Sigma                  #
#####################################################################
######               Importacion de librerias                 #######
import time
import threading
import credentials
import LCD
import bat
### Inicializacion de LCD
LCD.lcd_init()
LCD.lcd_clean()
LCD.lcd_fondo(100, 100, 100)
LCD.lcd_string("Iniciando...", 1, 1)
import numpy as np
import seg
import funciones
import HxNukak as hn
import filtros as fil
from math import floor
import backup
#####################################################################
######               Declaracion de variables                 #######
online = 0
estomaInfo = credentials.Get_Estoma_Info()
estoma_id = estomaInfo[0]
parametros = credentials.Get_Parametros_Filtro()
print parametros
tara = 0
intentosConexion = 0
testConex = 5
bandejaId = 0
peso_pm = 0
id_pm = 0
state = ""
rango = 100
alerta = 1
global pmarque
tiempoEnvio = 5
estado = 0
#### Nuevas
hxDebug = False
inicializacion = False
cantValidaciones = 0
MAXVALIDACIONES = 2
SUBJECT_CERO = "Error en cero"
BODY_CERO = "El equipo "+ estoma_id + " presenta variacion en el cero de: "
SUBJECT_VALIDACION = "Error en validacion"
BODY_VALIDACION = "El equipo " + estoma_id + " presenta problemas validando, error: "


#####################################################################
######               Declaracion de pines                     #######
botonUp = 26
botonDown = 16


#####################################################################
######               Hilos de ejecucion                       #######
#### Hilo de lectura de peso
class GetPeso(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.intentosMax = 3
        self.reboot = 0
        self.lenVec = parametros[0]
        self.lenVecFilP = parametros[1]
        self.vec = []
        self.alfa = parametros[2]
        self.alfaM = parametros[3]
        self.alfaG = parametros[4]
        self.difG = parametros[5]
        self.difM = parametros[6]
        self.resolucion = parametros[7]
        self.pesoEstable = -666  # Debe ser un negativo grande en magnitud
        self.rangoValido = 2
        self.pesoMin = 7
        self.embarque = 0
        self.embarqueId = 0
        self.sobrepeso = 0
        self.rango = pm.get_rango()
        self.estabilidad = 0.5
        self.time = time.time()
        self.tiempo = 0.5
        self.COLORTIME = 0.5
        self.COLORTIMEESTABLE = 0.1
        self.valorColorTime = time.time()
        self.SAVEDATA = True
        self.SHOWTIME = 0.15
        self.valorShowTime = time.time()
        self.primerShow = False
        self.primerEstable = False
        self.pesoSubida = 1
        self.tara = float(funciones.Get_parametro("tara"))
        self.CANTCALIBRACION = 1000
        self.cantBandejas = 0
        self.calibrar = False
        self.PATRON = float(funciones.Get_parametro("patron")) / 1000
        self.cantValidaciones = 0
        self.saveZero = True
        self.zeroInicial = []
        self.lenVecZero = 55
        for i in range(0, self.lenVecZero):
            self.zeroInicial.append(0)
        self.tiempoZero = time.time()
        self.vectorZero = []
        self.TIEMPOVALIDACIONZERO = 20
        self.zeroInit = 0
        self.auxZero = 0

    def Validar_Calibracion(self):
        print self.cantBandejas
        if self.cantBandejas >= self.CANTCALIBRACION:
            self.calibracion = -2
            self.calibrar = True
            pm.set_calibracion(self.calibracion)
            pm.set_estado(2)
            validacion = "pendiente"
        else:
            validacion = "no"
        while validacion =="pendiente":
            self.calibracion = pm.get_calibracion()
            if self.calibracion == -2:
                seg.nulo()
                LCD.Lcd_color("Azul")
                mensaje = str(
                    int(funciones.Get_parametro("patron")) / 1000) + "Kg y undir -->"  ###es int porque no cabe
                LCD.lcd_string("Poner patron de", 1, 1)
                LCD.lcd_string(mensaje, 2, 3)
                time.sleep(0.2)
                seg.full()
                time.sleep(0.2)
            elif self.calibracion == 0:
                self.Llenar_vector()
                LCD.lcd_clean()
                time.sleep(0.02)
                LCD.lcd_string("Validando...", 1, 1)
                self.cantValidaciones += 1
                while not self.Estable():
                    self.Get_lectura()
                    pesoValidacion = self.Get_peso()+self.tara
                    print pesoValidacion
                print "Patron: ", self.PATRON, "PesoValidacion: ", pesoValidacion
                print "Validacion: ", abs(pesoValidacion - self.PATRON)
                if abs(pesoValidacion - self.PATRON) > 0.1:
                    if self.cantValidaciones < MAXVALIDACIONES:
                        self.calibracion = -2
                        pm.set_calibracion(self.calibracion)
                        pm.set_estado(2)
                        LCD.lcd_string("Equipo", 1, 2)
                        LCD.lcd_string("descalibrado", 2, 2)
                        time.sleep(0.5)
                        LCD.lcd_string("Se intentara", 1, 2)
                        LCD.lcd_string("nuevamente", 2, 2)
                        time.sleep(0.5)
                    else:
                        funciones.Web_noti(SUBJECT_VALIDACION, BODY_VALIDACION)
                        while True:
                            LCD.lcd_string("Equipo averiado,", 1, 2)
                            LCD.lcd_string("contacte a SIOMA", 2, 2)
                elif abs(pesoValidacion - self.PATRON) <= 0.1:
                    self.calibracion = 4
                    pm.set_calibracion(self.calibracion)
                    pm.set_estado(4)
                    pm.Set_peso(pesoValidacion)

            elif self.calibracion == 4:
                while True:
                    calibracion = pm.get_calibracion()
                    if calibracion == 4:
                        self.Get_lectura()
                        peso = self.Get_peso() + self.tara
                        while not self.Estable():
                            self.Get_lectura()
                            peso = self.Get_peso() + self.tara
                            print peso
                        peso = floor(peso / resolucion) * resolucion
                        seg.Show_peso(peso, lb)
                        pm.Set_peso(peso)
                        if not pm.Get_alerta():
                            LCD.lcd_string("Nukak validado", 1, 2)
                            LCD.lcd_string("Continuar -->", 2, 3)
                    else:
                        break
            else:
                validacion = "bien"
                pm.set_estado(1)
                pm.init_embarques()
                print "Validacion realizada, el equipo se encuentra operando correctamente"

    def Llenar_vector(self):
        self.vec = []
        for i in range(0, self.lenVec):
            lectura = sensor.Get_lectura()/self.resolucion
            if not lectura:
                LCD.lcd_clean()
                LCD.lcd_string("REINICIE", 1, 2)
                while True:
                    pass
            self.vec.append(lectura)

    def Get_lectura(self):
        self.vec = funciones.Shift(self.vec, sensor.Get_lectura())
        if self.SAVEDATA:
            funciones.Log_datos(self.vec[self.lenVec-1], "/home/pi/datos/logData/SinFiltro.txt")
        self.vec[self.lenVec - self.lenVecFilP:self.lenVec] = fil.Filtro_picos(
            self.vec[self.lenVec - self.lenVecFilP:self.lenVec])
        self.vec[self.lenVec - self.lenVecFilP:self.lenVec] = fil.Filtro_picos(
            self.vec[self.lenVec - self.lenVecFilP:self.lenVec], vg=self.difG, pd=0.7)
        self.vec[self.lenVec - self.lenVecFilP - 1] = fil.Filtro_pasa_bajas(self.vec[self.lenVec - self.lenVecFilP - 1],
                                                                            self.vec[self.lenVec - self.lenVecFilP - 2],
                                                                            self.alfa, self.alfaM, self.alfaG,
                                                                            self.difM, self.difG)
        if self.SAVEDATA:
            tFile = open('/sys/class/thermal/thermal_zone0/temp')
            temp = float(tFile.read())
            tempC = temp/1000
            funciones.Log_datos(tempC, "/home/pi/datos/logData/temp.txt")
            funciones.Log_datos(self.vec[self.lenVec - self.lenVecFilP - 1], "/home/pi/datos/logData/ConFiltro.txt")

    def Show_peso(self, peso):
        if self.primerShow:
            if self.primerEstable:
                self.primerShow = False
                if peso < 1:
                    peso = -self.tara
                seg.Show_peso(peso, pm.get_lb())
            elif fil.Estable(self.vec[:self.lenVec - self.lenVecFilP], 0.5):
                self.primerShow = False
                if peso < 1:
                    peso = -self.tara
                seg.Show_peso(peso, pm.get_lb())
            else:
                seg.Show_peso(-10.00)
        else:
            if peso < 1:
                peso = -self.tara
            seg.Show_peso(peso, pm.get_lb())

    def Estable(self):
        return fil.Estable(self.vec[:self.lenVec - self.lenVecFilP], self.resolucion * self.estabilidad)

    def Get_peso(self):
        std = np.std(self.vec[:self.lenVec - self.lenVecFilP])
        prom = np.mean(self.vec[:self.lenVec - self.lenVecFilP])
        vMax = prom + std
        vMin = prom - std
        vecAux = []
        for i in range(0, self.lenVec - self.lenVecFilP):
            valor = self.vec[i]
            if vMin < valor < vMax:
                vecAux.append(valor)
        return floor(np.mean(vecAux)/self.resolucion) * self.resolucion

    def Validar_rango(self, pesoEstable):
        vecEmbarque = pm.Get_embarque()
        self.embarque = float(vecEmbarque[1])
        self.embarqueId = int(vecEmbarque[0])
        if self.embarque == 0:
            if pesoEstable > self.pesoMin:
                return True
            else:
                return False
        else:
            if abs(self.embarque - pesoEstable) <= self.rangoValido:
                return True
            else:
                return False

    def Guardar_peso(self, pesoEstable):
        self.pesoEstable = pesoEstable
        self.time = time.time()
        if self.embarque == 0:
            self.sobrepeso = 0
        else:
            self.sobrepeso = self.pesoEstable - self.embarque

    def Color_lcd_estable(self):
        sp = self.sobrepeso
        if self.pesoEstable < self.embarque:
            LCD.Lcd_color("Rojo")
        elif round(sp, 2) > round(self.rango, 2):
            LCD.Lcd_color("Morado")
        else:
            LCD.Lcd_color("Verde")

    def Color_lcd_inestable(self):
        LCD.Lcd_color("Azul")

    def Bajada(self):
        colaUp = np.mean(self.vec[:3])
        colaDown = np.mean(self.vec[self.lenVec - self.lenVecFilP - 3:self.lenVec - self.lenVecFilP])
        if colaUp > self.pesoMin > colaDown:
            return True
        else:
            return False

    def Subida(self):
        colaUp = np.mean(self.vec[:3])
        colaDown = np.mean(self.vec[self.lenVec - self.lenVecFilP - 3:self.lenVec - self.lenVecFilP])
        if colaUp < self.pesoSubida < colaDown:
            return True
        else:
            return False

    def Update_db(self):
        if self.pesoEstable > 0:
            funciones.Update_db(self.pesoEstable, pm.get_tara(),
                                self.embarqueId, self.sobrepeso)
            self.cantBandejas += 1
            self.Validar_Calibracion()
            self.pesoEstable = -666

    def Validar_cero(self, zeroActual):
        #print zeroActual
        try:
            if len(zeroActual) % 2 == 0:
                n = len(zeroActual)
                ValorZeroActual = (zeroActual[n/2-1] + zeroActual[n/2])/2
            else:
                ValorZeroActual = zeroActual[len(zeroActual)/2]

            print "Cero inicial: ", self.zeroInicial[len(self.zeroInicial)/2], "Cero actual: ", ValorZeroActual
            error = abs(self.zeroInicial[len(self.zeroInicial)/2] - ValorZeroActual)
            print "Error: ", error
            if error > 0.05:
                print funciones.Web_noti(SUBJECT_CERO, BODY_CERO + str(error))
        except Exception, e:
            print "ERROR VALIDANDO CERO: ", repr(e)


    def run(self):
        self.Llenar_vector()

        while True:

            self.Get_lectura()

            if self.zeroInit == 0:
                if self.vec[self.lenVec - self.lenVecFilP - 1] < 0:
                    self.zeroInicial = funciones.Shift(self.zeroInicial, self.vec[self.lenVec - self.lenVecFilP - 1])

                elif self.vec[self.lenVec - self.lenVecFilP - 1] > 0:
                    self.zeroInit = 1
                    print self.zeroInicial
                    self.zeroInicial.sort()
                    print "Cero inicial: ", self.zeroInicial[len(self.zeroInicial)/2]
                    funciones.Set_parametro("cero", self.zeroInicial[len(self.zeroInicial)/2])

            peso = self.Get_peso()

            if self.vec[self.lenVec - self.lenVecFilP - 1] < 0 and self.zeroInit == 1:
                self.vectorZero.append(self.vec[self.lenVec - self.lenVecFilP - 1])

            if time.time()-self.valorShowTime > self.SHOWTIME:
                self.valorShowTime = time.time()
                self.Show_peso(peso)

            if self.Estable():
                if self.primerShow:
                    self.primerEstable = True
                pesoEstable = peso
                if self.Validar_rango(pesoEstable) and time.time() - self.time > self.tiempo:
                    self.Guardar_peso(pesoEstable)
                    if time.time() - self.valorColorTime > self.COLORTIMEESTABLE:
                        self.valorColorTime = time.time()
                        self.Color_lcd_estable()
            else:
                if time.time() - self.valorColorTime > self.COLORTIME:
                    self.valorColorTime = time.time()
                    self.Color_lcd_inestable()
            if self.Bajada():
                self.primerEstable = False
                self.Update_db()
                self.auxZero = 0
                self.tiempoZero = time.time()
            if self.Subida():
                if self.auxZero == 0:
                    if (time.time() - self.tiempoZero) > self.TIEMPOVALIDACIONZERO:
                        self.Validar_cero(self.vectorZero)
                        self.vectorZero = []
                        self.auxZero = 1
                self.primerShow = True



### Hilo de subida de datos a la web
class UpLoad(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        while True:
            try:
                # Ejecutar la funcion de subida de datos a la web
                funciones.Sync()
            # print JsonData()
            except Exception, e:
                print "ERROR: Caught exception: " + repr(e)
                print "Sin conexion a internet"

            time.sleep(tiempoEnvio)


#####################################################################
######               Metodos de inicializacion                #######
### Validacion de la conexion a internet
while (online == 0) & (intentosConexion < testConex):
    try:
        online = funciones.Test_online()
        if online == 0:
            intentosConexion += 1
        print intentosConexion
    except:
        LCD.Lcd_color("Morado")
        intentosConexion = intentosConexion + 1
        print intentosConexion


hora = funciones.Cmd_line("date")
rtc = funciones.Cmd_line("sudo hwclock -r")
print "RTC: ", rtc
print "Hora sistema: ", hora
if online == 0:
    print "Sistema sin internet, se usara hora de RTC"
    funciones.Cmd_line("sudo hwclock -w")

time.sleep(5)
import parametros as pm
import get_peso as gp
lb = pm.get_lb()
resolucion = parametros[7]
PATRON = float(funciones.Get_parametro("patron"))/1000
TARA = float(funciones.Get_parametro("tara"))
try:
    print "Iniciando................."
    ### Inicializacion de sensor de peso
    sensor = hn.HXNUKAK(debug=hxDebug, tara=TARA, ratio=float(funciones.Get_parametro("ratio")))
    ### Inicializacion de base de datos
    funciones.Conect_db()

    ### Inicializacion de LCD
    LCD.lcd_init()
    LCD.lcd_clean()
    LCD.lcd_fondo(100, 100, 100)
    print "Iniciado"
except Exception, e:
    print "ERROR INICIALIZANDO: " + repr(e)

### Flujo de inicio de operacion
calibrar = int(funciones.Get_parametro("calibrar"))
if calibrar == 1:
    print "Se va a calibrar"
    pm.set_calibracion(2)
pm.set_estado(estado)
testPatron = gp.GetPeso(sensor=sensor)

### Validacion de inicio
while (estado == 0):
    calibracion = pm.get_calibracion()
    if calibracion == 0:
        pm.set_estado(0)
        LCD.lcd_string("<--Validar", 1, 1)
        LCD.lcd_string("Iniciar-->", 2, 3)
        LCD.lcd_fondo(100, 100, 100)
    elif calibracion > 0:
        if calibracion == 1:
            time.sleep(0.5)
            pm.set_estado(2)
            LCD.lcd_string("Validar?", 1, 2)
            LCD.lcd_string("<--Si      No-->", 2, 2)
        elif calibracion == 2:
            time.sleep(0.5)
            pm.set_estado(3)
            # print "Calibrando"
            if calibrar == 1:
                mensaje = str(20) + "Kg y undir -->"
            else:
                mensaje = str(int(PATRON)) + "Kg y undir -->"  ###es int porque no cabe
            # print mensaje, len(mensaje)
            LCD.lcd_string("Poner patron de", 1, 1)
            LCD.lcd_string(mensaje, 2, 3)
        elif calibracion == 3:
            LCD.lcd_clean()
            time.sleep(0.02)
            if calibrar == 1:
                LCD.lcd_string("Calibrando...", 1, 1)
            else:
                LCD.lcd_string("Validando...", 1, 1)
            pm.set_estado(4)
            cantValidaciones += 1
            if calibrar == 1:
                calibracion, auxEstado = sensor.Calibrar_nukak()
            else:
                pesoValidacion = testPatron.Validar()
                pesoValidacion += TARA
                print "Peso: ", pesoValidacion, "Patron: ", PATRON
                print "Diferencia de validacion: ", pesoValidacion - PATRON
                difernecia = abs(pesoValidacion - PATRON)
                if difernecia > 0.1:
                    if cantValidaciones < MAXVALIDACIONES:
                        calibracion, auxEstado = 2, 2
                        LCD.lcd_string("Equipo", 1, 2)
                        LCD.lcd_string("descalibrado", 2, 2)
                        time.sleep(0.5)
                        LCD.lcd_string("Se intentara", 1, 2)
                        LCD.lcd_string("nuevamente", 2, 2)
                        time.sleep(0.5)
                    else:
                        funciones.Web_noti(SUBJECT_VALIDACION, BODY_VALIDACION + str(difernecia))
                        while True:
                            LCD.lcd_string("Equipo averiado,", 1, 2)
                            LCD.lcd_string("contacte a SIOMA", 2, 2)
                elif difernecia <= 0.1:
                    calibracion, auxEstado = 4, 4
                    funciones.Insert_Validacion(1, funciones.Actualizar_hora(), PATRON / 1000, 0, funciones.Get_parametro("ratio"), pesoValidacion)
            if auxEstado == 2:
                pm.set_estado(auxEstado)
            pm.set_calibracion(calibracion)

            time.sleep(0.2)
            #print("Ratio actualizado")
        elif calibracion == 4:
            testPatron.Llenar_vector()
            while True:
                calibracion = pm.get_calibracion()
                if calibracion == 4:
                    peso = testPatron.Execute()
                    peso = floor(peso / resolucion) * resolucion
                    seg.Show_peso(peso, lb)
                    pm.Set_peso(peso)
                    if not pm.Get_alerta():
                        if calibrar == 1:
                            LCD.lcd_string("Nukak calibrado", 1, 2)
                            LCD.lcd_string("Continuar -->", 2, 3)
                        else:
                            LCD.lcd_string("Nukak validado", 1, 2)
                            LCD.lcd_string("Continuar -->", 2, 3)
                else:
                    break
    else:
        estado = 1
        pm.set_estado(estado)

### Inicializacion de pmarques
pm.init_embarques()

### Inicializacion de hilos
Bascula = GetPeso()
upload = UpLoad()
### Se inicia el hilo de lectura serial
Bascula.start()
### Se inicia el hilo de subida de datos a la web
if int(funciones.Get_parametro("subir_datos")) == 1:
    upload.start()
else:
    print "No se subiran datos al servidor"
