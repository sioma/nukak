#!/bin/bash
g="\033[0;32m"
n="\033[0m"
echo -e "${g}Crenado carpetas de backup..........................${n}"
sudo mkdir /home/pi/datos
sudo mkdir /home/pi/datos/db
sudo mkdir /home/pi/datos/logData
sudo chown pi /home/pi/datos -R
sudo chown postgres /home/pi/datos/db
sudo chown pi /firmware -R
sudo PGPASSWORD='sioma' psql -U postgres -w -d estomadb -c "DROP TABLE embarques;"
echo -e "${g}Parche finalizado, el dispositivo se encuentra actualizado..........................${n}"