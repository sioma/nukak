# -*- coding: utf-8 -*-
#####################     SIOMA S.A.S    ############################
#                     Libreria de filtros                           #
#####################################################################
######               Importacion de librerias                 #######
import numpy as np


#####################################################################
#####################################################################
######                        Funciones                       #######
#### Funcion para filtrar picos y ondas cuadradas
def Filtro_picos(vector, vg=5, pd=0.8):
    vg = vg  # Diferencia para determinar el uso del filtro
    pd = pd  # Porcentaje de diferencia para verificar onda cuadrada
    it = len(vector)
    dif = vector[it - 1] - vector[it - 2]
    for i in range(2, it):
        dif2 = vector[it - i] - vector[it - i - 1]
        if dif != 0 and dif2 != 0 and (abs(dif) > vg):
            if (abs(abs(dif) - abs(dif2))) / (max(abs(dif), abs(dif2))) < pd:
                m = (vector[it-1]-vector[it-i-1])/i
                for j in range(it-i, it-1):
                    vector[j] = vector[j-1] + m
    return vector

#### Funcion para hacer un filtro pasa bajas
def Filtro_pasa_bajas(actual, anterior, alfa, alfaM, alfaG, difG, difM):
    difA = abs(actual - anterior)
    if difA >= difG:
        alfa = alfaG
    elif difA >= difM:
	alfa = alfaM
    return alfa * (actual) + ((1 - alfa) * anterior)

#### Funcion para determinar la estabilidad
def Estable (vec, estabilidad):
    promedio=np.average(vec)
    vectorDif=[]
    for i in range(0, len(vec)):
        vectorDif.append(abs(vec[i] - promedio))
    if max(vectorDif) < estabilidad:
        return True
    else:
        return False
